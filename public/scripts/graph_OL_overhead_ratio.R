#! /usr/bin/env -S Rscript --no-save

library(ggplot2)
library(tidyr)
library(stringr)
library(dplyr)
library(scales)

INTERPOLATE<-FALSE
BIG_LEGEND<-TRUE

time_label <- function(x_f){
  labels <- ""
  for( i in x_f){
  const_val=c("µs","ms","s")
  x <- as.numeric(as.character(i))
  e=0
  while (x >= 10)
  {
    x=x/10
    e=e+1
  }
  m <- (e%%3)
  u <- e%/%3
  labels <- paste(labels,round(x*10^m,1),const_val[u+1]," ",sep="")
  }
  return (unlist(strsplit(labels, split=" "))) 
}

size_label <- function(x_f){
  labels <- ""
  for( i in x_f){
  const_val=c("o","ko","Mo","Go")
  x <- as.numeric(as.character(i))
  e=0
  while (x >= 10)
  {
    x=x/10
    e=e+1
  }
  m <- (e%%3)
  u <- e%/%3
  labels <- paste(labels,round(x*10^m,1),const_val[u+1]," ",sep="")
  }
  return (unlist(strsplit(labels, split=" "))) 
}

args <- commandArgs(trailingOnly=TRUE)
ref_tab <- lapply(args,read.table,header=FALSE,fill=TRUE,nrows=12)
ref_tab2 <- lapply(args,read.table,header=FALSE,fill=TRUE,skip=14,nrows=12)
df_tab <- lapply(args,read.table,header=FALSE,fill=TRUE,skip=44)
implem <- gsub("^.*\\/","", args)
title_tab <- unlist(gsub("\\_raw.*","",implem))
title_tab <- lapply(title_tab,str_replace_all,pattern="_",replacement=" ")
i=1
for(temp in df_tab){
df = data.frame(temp)
df2 = data.frame(ref_tab[i])
df3 = data.frame(ref_tab2[i])
#print(df2)
#print(df3)
df$temp_comm = as.vector(sapply(as.vector(df$V1[seq(1, length(df$V1), 13)]), function (x) rep(x,13)))
df <- df[-seq(1, length(df$V1), 13),]
df <- extract(df,temp_comm, c("data_size","dt_time","dr_time"), "\\[([[:digit:]]+)-([[:digit:]]+)-([[:digit:]]+\\.[[:digit:]]+)\\]", convert=TRUE)
df <- extract(df,V1, c("mt_time","matrix_size","mr_time"), "\\[([[:digit:]]+)-([[:digit:]]+)-([[:digit:]]+\\.[[:digit:]]+)\\]", convert=TRUE)
  colnames(df) <- c("mt_time","matrix_size","mr_time","tcall_min","tcalc_min","twait_min","ttotal_min","tcall_max","tcalc_max","twait_max","ttotal_max","tcall","tcalc","twait","ttotal","overhead","data_size","dt_time","dr_time")
df$dr_time = rep(df2$V4, each =12)
df$mr_time = df3$V6
#print(data.frame(df$mr_time,df3$V6))
df$prop_wait = round(df$twait/df$dr_time*100,2)
df$prop_wait <- squish(df$prop_wait,c(0,200))
df$oh<-round((df$ttotal_max - pmax(df$dr_time,df$mr_time))/pmin(df$dr_time,df$mr_time),2)
#df$oh<-round((df$ttotal_max - pmax((df$twait_max+df$tcall_max),df$tcalc_max))/pmin((df$twait_max+df$tcall_max),df$tcalc_max),2)
df$matrix_size <- as.factor(df$matrix_size)
df$dt_time <- as.factor(df$dt_time)
df$mt_time <- as.factor(df$mt_time)
df$dr_time <- as.factor(df$dr_time)
df$mr_time <- as.factor(df$mr_time)
uds <- unique(as.vector(df[c("data_size", "dt_time")]$data_size))
#print(unique(df$data_size))
#print(uds)
#print(df, row.names=FALSE)
if(BIG_LEGEND)
{ 
  lblsize <- 27
  labelx <-paste(time_label(as.vector(unique(df$dt_time))),sep="\n") 
  labely <- paste(time_label(as.vector(unique(df$mt_time))),sep="\n")
  xname <- "Collective time"
  sub = NULL
}else{
  xname <- "Collective size and times"
  lblsize <- 10
  sub <- paste("File: ",args[i],sep="")
labelx <- paste(size_label(as.vector(uds)),time_label(as.vector(unique(df$dt_time))),time_label(as.vector(unique(df$dr_time))),sep="\n")
labely <- paste(unlist(as.vector(unique(df$matrix_size))),time_label(as.vector(unique(df$mt_time))),time_label(as.vector(unique(df$mr_time))),sep="\n")
}
g <- ggplot(df, aes(x=dt_time, y=mt_time))+
  labs(title = title_tab[i], subtitle=sub)+
  theme(plot.title = element_text(hjust = 0.5))+
  theme(plot.subtitle = element_text(hjust = 0.5))+
	geom_raster(aes(fill=df$oh),interpolate=INTERPOLATE)+
  theme(panel.background=element_rect(fill="transparent",colour=NA))+
  theme(panel.grid.major = element_line(colour = "black",size=0.01))+
  scale_x_discrete(name=xname,breaks=unique(df$dt_time), labels=labelx)+
  scale_y_discrete(name="Matrix compute time",breaks=unique(df$mt_time), labels=labely)+
  scale_fill_gradientn(colours=c("dodgerblue3","green3","gold","red3"),oob=scales::squish,breaks=c(-1,0,1,2),values=scales::rescale(c(-1,0,1,2),to=c(0,1)),labels=c(expression(""<="-1.0"),"0.0","1.0",expression("">="2.0")),minor_breaks=NULL,space = "Lab",na.value = "#00000000",name="overhead" ,guide = guide_colourbar(direction="vertical"), aesthetics = "fill",limits=c(-1,2))+
  
  theme(legend.position="right",title=element_text(size = 20),legend.title=element_text(size = 20),legend.text=element_text(size = 18),axis.text=element_text(size = lblsize),axis.text.x=element_text(angle=-90), axis.title=element_text(size = 20))
name <- str_remove(args[i],"raw.dat")
file2 <-paste(name,"OL_overhead_ratio.png",sep="")
i=i+1
ggsave(file2, width = 9, height = 8, dpi = 150)
}
